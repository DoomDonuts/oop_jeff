﻿using UnityEngine;
using System.Collections;

public class BulletBehaviour : MonoBehaviour 
{
	public float speed;
	public string enemyTag;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Translate(Vector3.forward * speed * Time.deltaTime);
	}
}
