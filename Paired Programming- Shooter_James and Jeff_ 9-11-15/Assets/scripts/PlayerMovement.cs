﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	public float speed;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	movement ();
	}
	public void movement()
	{
		if (Input.GetKey (KeyCode.W))
		{
			transform.Translate(Vector3.forward * speed * Time.deltaTime);
			transform.Rotate(0,0,0);
		}
		if (Input.GetKey (KeyCode.A))
		{
			transform.Translate(Vector3.left * speed * Time.deltaTime);
			transform.Rotate(0,1,0);
		}
		if (Input.GetKey (KeyCode.S))
		{
			transform.Translate(Vector3.back * speed * Time.deltaTime);
			transform.Rotate(0,0,0);
		}
		if (Input.GetKey (KeyCode.D))
		{
			transform.Translate(Vector3.right * speed * Time.deltaTime);
			transform.Rotate(0,-1,0);
		}
	
		
	}
	
}
