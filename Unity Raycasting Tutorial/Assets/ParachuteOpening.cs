﻿using UnityEngine;
using System.Collections;

public class ParachuteOpening : MonoBehaviour 
{
	public float parachuteEffectiveness;
	public float deploymentHeight;
	private bool deployed;
	public Rigidbody myRigidbody;
	
	// Update is called once per frame
	void Update () 
	{
		RaycastHit hit;
		Ray landingRay = new Ray(transform.position, Vector3.down);
		
		Debug.DrawRay(transform.position, Vector3.down * deploymentHeight);
		
		if(!deployed)
		{
			if(Physics.Raycast(landingRay, out hit, deploymentHeight))
			{
				if(hit.collider.tag == "Ground")
				{
					DeployParachute();
				}
			}
		}
	}
	
	public void DeployParachute()
	{
		deployed = true;
		myRigidbody.drag = parachuteEffectiveness;
	}
}
