﻿using UnityEngine;
using System.Collections;

public class ArrayRevision : MonoBehaviour 
{
	public int[] collectiveAges;
	public int arraySize;
	public int currentIndex;

	// Use this for initialization
	void Start () 
	{
	  collectiveAges = new int[arraySize];
	  SetInitialValues();
	}
	
	void Update ()
	{
	   CheckKeys();
	   
	}
	
	public void SetInitialValues()
	{
		for(int index = 0; index < collectiveAges.Length; index++) // can replace collectiveAges.Length with the arraySize int variable.
		{
			collectiveAges[index] = -1;
		}
	}
	
	public void CheckKeys() 
	{
	   if(Input.GetKeyDown(KeyCode.UpArrow))
	   {
	       print ("Up Arrow was pressed");
	       IncreaseIndex();
	   }
	   
	   if(Input.GetKeyDown(KeyCode.DownArrow))
	   {
	       print ("Down Arrow was pressed");
		   DecreaseIndex();
	   }
	   
	   if(Input.GetKeyDown(KeyCode.Return))
	   {
	       SetAge();
	   }
	}
	
	public void SetAge()
	{
	   collectiveAges[currentIndex] = 20;
	}
	
	public void DecreaseIndex()
	{
	  currentIndex -= 1; //can use currentIndex--;  or currentIndex - 1;
	  if(currentIndex < 0)
	  {
	     currentIndex = 0;
	     Debug.LogWarning("Negative numbers are out of range! CurrentIndex set to 0");
	  }
	}
	
	public void IncreaseIndex()
	{
		currentIndex += 1; //can use currentIndex++;  or currentIndex + 1;
		if(currentIndex > collectiveAges.Length - 1)
		{
		   currentIndex = collectiveAges.Length - 1;
		   Debug.LogWarning("Number exceeded last element number! CurrentIndex set to size - 1");
		}
	}
}
