﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct item
{
  public string name;
  public string description;
  public string useMessage;
}


public class PlayerInventory : MonoBehaviour 
{
    //This is task 10 of OOP Lesson 5
    
    public item[] itemArray;
    public int currentIndex;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	   IncreaseIndex();
	   DecreaseIndex();
	   UseItem();
	   CheckKeys();
	}
	
	public void IncreaseIndex()
	{
	   if(Input.GetKeyDown(KeyCode.UpArrow) && currentIndex < itemArray.Length - 1)
	   {
	      currentIndex++;
	   }
	}
	
	public void DecreaseIndex()
	{
	   if(Input.GetKeyDown(KeyCode.DownArrow) && currentIndex > 0)
	   {
	      currentIndex--;
	   }
	}
	
	public void UseItem()
	{
	   if(Input.GetKeyDown(KeyCode.Space))
	   {
	     print (itemArray[currentIndex].useMessage); 
	   }
	}
	
	public void CheckKeys()
	{
	   if(Input.GetKeyDown(KeyCode.LeftBracket))
	   {
	      print ("Left bracket was pressed.");
	      if(currentIndex > 0)
	      {
	        currentIndex--;
	      }  
	   }
	   
	   if(Input.GetKeyDown(KeyCode.RightBracket))
	   {
	      print ("Right bracket was pressed.");
	      if(currentIndex < itemArray.Length - 1)
	      {
	        currentIndex++;
	      }  
	   }
	}
}
