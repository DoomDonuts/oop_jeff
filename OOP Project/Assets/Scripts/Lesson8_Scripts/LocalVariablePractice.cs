﻿using UnityEngine;
using System.Collections;

public class LocalVariablePractice : MonoBehaviour 
{
	public string playerName;
	public string sentence;
	public GameObject lastCollidedObject;

	// Use this for initialization
	void Start () 
	{
		SetPlayerName();
	}
	
	// Update is called once per frame
	void Update () 
	{
		CreateSentence();
		CreateDungeonMessage();
	}
	
	public void SetPlayerName()
	{
		string tempName;
		tempName = "Player 2"; 
		playerName = "Player 1";
	}
	
	public void CreateSentence()
	{
		string sentence;
		sentence = ""; //assigns an empty string, so that it isn't null
		if(Input.GetKeyDown(KeyCode.Alpha1))
		{
			sentence += "My name is " + playerName;
		}
		
		if(Input.GetKeyDown(KeyCode.Alpha2))
		{
			sentence += "\n"; //makes a new line in the string 
			sentence += "One day I died. The End"; 
		}
		
		if(sentence.Length > 0) // are there any letters in the sentence
		{
			print(sentence);
		}
	}
	
	public void CreateDungeonMessage()
	{
		string dungeonMessage;
		dungeonMessage = "";
		if(Input.GetKeyDown(KeyCode.I))
		{
			string message;
			message = "You have entered a dungeon\n";
			dungeonMessage += message;
		}
		
		if(Input.GetKeyDown(KeyCode.O))
		{
			string message;
			message = "You see a dragon!\n";
			dungeonMessage += message;
		}
		
		if(Input.GetKeyDown(KeyCode.P))
		{
			string message;
			message = "You are fucked\n";
			dungeonMessage += message;
		}
		
		if(Input.GetKeyDown(KeyCode.U))
		{
			string message;
			message = "You are the champion!\n";
			dungeonMessage += message;
		}
		
		if(Input.GetKeyDown(KeyCode.Return))
		{
			//print(dungeonMessage);
			//dungeonMessage = ""; 
			print (sentence);
			sentence = ""; //clears the value of this variable so another value can be inputted
		}
		
		if(dungeonMessage.Length > 0)
		{
			sentence += dungeonMessage;
		}
	}
	
	public void OnCollisionEnter(Collision col)
	{
		lastCollidedObject = col.gameObject;
	}
}
