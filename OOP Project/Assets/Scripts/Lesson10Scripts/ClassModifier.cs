﻿using UnityEngine;
using System.Collections;

public class ClassModifier : MonoBehaviour 
{
	public ClassPractice otherClass;

	// Use this for initialization
	void Start () 
	{
		//otherClass = GetComponent<ClassPractice>();
		otherClass.PrintNumber();
		otherClass.PrintDecimalNumber();
		otherClass.PrintABoolean();
		otherClass.PrintAWord();
		otherClass.PrintAnotherWord();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
