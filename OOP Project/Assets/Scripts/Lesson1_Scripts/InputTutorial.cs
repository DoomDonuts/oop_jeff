﻿using UnityEngine;
using System.Collections;

public class InputTutorial : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	   if (Input.GetKeyDown (KeyCode.W)) 
		{
			print ("W key was pressed");
		}

		if (Input.GetKeyDown (KeyCode.A)) 
		{
			print ("A key was pressed");
		}

		if (Input.GetKeyDown (KeyCode.S)) 
		{
			print ("S key was pressed");
		}

		if (Input.GetKeyDown (KeyCode.D)) 
		{
			print ("D key was pressed");
		}
	}
}
