﻿using UnityEngine;
using System.Collections;

public class PlayerJump : MonoBehaviour 
{
	public float currentJumpForce;
	public float normalJumpForce;
	public bool canJump;
	public Rigidbody myRB;
	public string floorTag;
	public string obstacleTag;
	
	public float timeAirbourne;
	public float timeAirbourneLimit;
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKey(KeyCode.Space) && canJump)
		{
			myRB.AddForce(Vector3.up * currentJumpForce);
			currentJumpForce += 0.5f;
			timeAirbourne += Time.deltaTime;
			if(timeAirbourne >= timeAirbourneLimit)
			{
				canJump = false;
				timeAirbourne = 0;
				currentJumpForce = normalJumpForce;
			}
		}
		if(Input.GetKeyUp(KeyCode.Space))
		{
			canJump = false;
		}
	}
	
	public void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.tag == floorTag || other.gameObject.tag == obstacleTag)
		{
			canJump = true;
		}
	}
}
