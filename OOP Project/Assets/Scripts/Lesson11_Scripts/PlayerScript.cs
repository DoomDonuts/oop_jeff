﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour 
{
	public float currentPlayerSpeed;
	public float normalPlayerSpeed;
	public float slowPlayerSpeed;
	
	public float currentTime;
	public bool hitObstacle;
	
	public string obstacleTag;
	public string deadZoneTag;
	public Rigidbody myRB;
	
	void Start () 
	{
		myRB = GetComponent<Rigidbody>();
		currentPlayerSpeed = normalPlayerSpeed;
	}
	
	void Update () 
	{
		MovePlayer();
		if(hitObstacle)
		{
			StartTimer();
		}
	}
	
	public void MovePlayer()
	{
		myRB.AddForce(Vector3.forward * currentPlayerSpeed);
	}
	
	public void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == obstacleTag)
		{
		    Destroy(other.gameObject);
		    currentPlayerSpeed = slowPlayerSpeed;
			hitObstacle = true;
		}
		
		if(other.gameObject.tag == deadZoneTag)
		{
			Application.LoadLevel ("Lesson11");
		}
	}
	
	public void StartTimer()
	{
		currentTime += Time.deltaTime;
		if(currentTime >= 3)
		{
			currentPlayerSpeed = normalPlayerSpeed;
			currentTime = 0;
			hitObstacle = false;
		}
	}
}
