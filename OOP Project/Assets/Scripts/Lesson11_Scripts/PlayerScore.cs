﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerScore : MonoBehaviour 
{
	public int currentScore;
	public float currentTime;
	public float timeLimit;
	public Text scoreText;
	
	// Update is called once per frame
	void Update () 
	{
		scoreText.text = "Score: " + currentScore;
		currentTime += Time.deltaTime;
		if(currentTime >= timeLimit)
		{
			currentScore++;
			currentTime = 0;
		}
	}
}
