﻿using UnityEngine;
using System.Collections;

public class PlatformManager : MonoBehaviour 
{
	public GameObject platformGroupA;
	public string playerTag;
	public Vector3 newPlatformPos;
	public float platformOffset;
	public Rigidbody playerRB;
	
	public void Update()
	{
		platformOffset = (playerRB.velocity.magnitude)/2;
	}
	
	public void ClonePlatforms()
	{
		Instantiate(platformGroupA, newPlatformPos * platformOffset, Quaternion.identity);
		newPlatformPos += new Vector3(0, 0, 300);
		platformGroupA.transform.position = newPlatformPos;
	}
	
	public void OnTriggerExit(Collider other)
	{
		if(other.gameObject.tag == playerTag)
		{
			ClonePlatforms();
		}
	}
}
