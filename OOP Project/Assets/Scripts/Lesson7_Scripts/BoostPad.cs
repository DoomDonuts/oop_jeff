﻿using UnityEngine;
using System.Collections;

public class BoostPad : MonoBehaviour 
{
    public float forceAmount = 0.0f;
    public Rigidbody playerRigidbody; // holds the player's rigidbody
    public string playerTag;
	
	//Jeff's code
	/*public void OnTriggerEnter(Collider other) 
	{
	   if(other.gameObject.tag == playerTag)
	   {
	     Boost(Vector3.right, forceAmount);
	   }
	}
	
	public void Boost(Vector3 direction, float forceAmount)
	{
	   playerRigidbody.AddForce(direction * forceAmount);
	} */
	
	//Moe's code
	public void OnTriggerEnter(Collider col)
	{
	   if(col.tag == playerTag) //check tag of object collided
	   {
	      playerRigidbody = col.GetComponent<Rigidbody>(); //gets the rigidbody of the object collided with
	      Boost(transform.forward, forceAmount);
	   }
	}
	
	public void Boost(Vector3 direction, float speed)
	{
	   playerRigidbody.AddForce(direction * speed, ForceMode.Impulse);
	}
}

