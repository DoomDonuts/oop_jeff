﻿using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour 
{
    public Rigidbody myRigidbody;
    public float speed = 0.0f;
   
	// Use this for initialization
	void Start () 
	{
	   myRigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () 
	{
	   checkKeys();
	   
	}
	
	public void moveForward()
	{
	   myRigidbody.AddForce(Vector3.forward * speed);
	}
	
	public void moveLeft()
	{
	   myRigidbody.AddForce(Vector3.left * speed);
	}
	
	public void moveBackward()
	{
	   myRigidbody.AddForce(Vector3.back * speed);
	}
	
	public void moveRight()
	{
	   myRigidbody.AddForce(Vector3.right * speed);
	}
	
	public void checkKeys()
	{
	  if(Input.GetKey(KeyCode.W))
	  {
	    //moveForward();
	    move (Vector3.forward,speed);
	  }
	  
	  if(Input.GetKey(KeyCode.A))
	  {
	    //moveLeft();
	    move (Vector3.left,speed);
	  }
	  
	  if(Input.GetKey(KeyCode.S))
	  {
	    //moveBackward();
	    move (Vector3.back,speed);
	  }
	  
	  if(Input.GetKey(KeyCode.D))
	  {
	    //moveRight();
	    move (Vector3.right,speed);
	  }
	  
	  if(Input.GetKey(KeyCode.Space))
	  {
	    move(Vector3.up,speed*10);
	  }
	  //checks for the keypresses w, a, s, d
	}
	
	private void move(Vector3 direction, float forceAmount)
	{
	   myRigidbody.AddForce(direction * forceAmount);
	}
}
