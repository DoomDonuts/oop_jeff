﻿using UnityEngine;
using System.Collections;

public class FunctionsRevisited : MonoBehaviour 
{
    public string myName;
    public int myAge;
    public bool isMale;
    
    public float inputOne;
    public float inputTwo;
    public float inputThree;
    public float inputFour;
    
    public float number1;
    public float number2;
    public float result;
    
	// Use this for initialization
	void Start () 
	{
	   //setName("Moe");
	   //setAge(9000);
	   //setGender(false);
	   setPersonalDetails("Daniel", 30, false); 
	   //subtractNumbers(inputOne, inputTwo);
	   //addNumbers(inputThree, inputFour);
	}
	
	// Update is called once per frame
	void Update () 
	{
	   checkKeys();
	}
	
	public void setName(string name)
	{
	   myName = name;
	}
	
	public void setAge(int age)
	{
	   myAge = age;
	}
	
	public void setGender(bool gender) 
	{
	   isMale = gender;
	}
	
	public void setPersonalDetails(string name, int age, bool gender)
	{
		setName(name);
		setAge(age);
		setGender(gender);
	}
	
	public void subtractNumbers(float num1, float num2)
	{
	   print(num1 - num2);
	   result = num1 - num2;
	}
	
	public void addNumbers(float num1, float num2)
	{
	   print (num1 + num2);
	   result = num1 + num2;
	}
	
	public void multiplyNumbers(float num1, float num2)
	{
		print(num1 * num2);
		result = num1 * num2;
	}
	
	public void divideNumbers(float num1, float num2)
	{
		print (num1 / num2);
		result = num1 / num2;
	}
	
	public void checkKeys()
	{
	   if(Input.GetKeyDown(KeyCode.KeypadPlus))
	   {
	     addNumbers(number1, number2);
	   }
	   
	   if(Input.GetKeyDown(KeyCode.KeypadMinus))
	   {
		 subtractNumbers(number1, number2);
	   }
	   
	   if(Input.GetKeyDown(KeyCode.KeypadMultiply))
	   {
	     multiplyNumbers(number1, number2);
	   }
	   
	   if(Input.GetKeyDown(KeyCode.KeypadDivide))
	   {
	     divideNumbers(number1, number2);
	   }
	}
}
