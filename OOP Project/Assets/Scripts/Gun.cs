﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour 
{
	public GameObject bulletPrefab;
	public float bulletSpeed;
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Return))
		{
			GameObject clone;
			Rigidbody cloneRB;
			clone = Instantiate(bulletPrefab, transform.position, Quaternion.identity) as GameObject;
			cloneRB = clone.GetComponent<Rigidbody>();
			cloneRB.AddRelativeForce(Vector3.forward * bulletSpeed,ForceMode.Impulse);
		}
	}
}
