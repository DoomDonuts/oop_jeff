﻿using UnityEngine;
using System.Collections;

public class Jetpack : MonoBehaviour 
{
	public float pushForce;
	public float maxFuel;
	public float currentFuel;
	public float drainRate;

	public Rigidbody rb;
	// Use this for initialization
	void Start ()
	{
		rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		Fly();
	}
	
	public void Fly()
	{
		if(Input.GetKey(KeyCode.Space))
		{
			rb.AddForce(Vector3.up * pushForce);
			currentFuel -= drainRate;
			if(currentFuel > 0)
			{
				//fall
				pushForce = 0;
				
			}
			else
			{
				pushForce = 25;
			}
		}
	}
}
