﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class bounyBAll : MonoBehaviour {
	
	public float bouncePower;
	private Rigidbody rb;
	
	// Use this for initialization
	void Start () {
	rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void OnCollisionEnter (Collision other) {
	rb.AddForce (Vector3.up * bouncePower,ForceMode.Impulse);
	}
}
