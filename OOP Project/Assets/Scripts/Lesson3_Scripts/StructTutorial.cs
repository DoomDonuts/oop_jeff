﻿using UnityEngine;
using System.Collections;

[System.Serializable] //makes the struct visible in the inspector
public struct address
{
	public int houseNumber;
	public string streetName;
	public string suburb;
	public int postcode;
	public string state;
	public string country;
}

public class StructTutorial : MonoBehaviour 
{
	public address myAddress;

	// Use this for initialization
	void Start () 
	{
		SetStartingAddress();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
			ChangeAddress();
		}
	}
	
	public void SetStartingAddress()
	{
		myAddress.houseNumber = 213;
		myAddress.streetName = "Pacific Highway";
		myAddress.suburb = "St Leonards";
		myAddress.postcode = 2086;
		myAddress.state = "NSW";
		myAddress.country = "Australia";
	}
	
	public void ChangeAddress()
	{
		myAddress.houseNumber = 123;
		myAddress.streetName = "Fake Street";
		myAddress.suburb = "Springfield";
		myAddress.postcode = 1000;
		myAddress.state = "???";
		myAddress.country = "Merica";
	}
}
