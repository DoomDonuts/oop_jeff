﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct pokemonStats
{
	string name;
	bool gender;
	int level;
	int pokedexNumber;
	bool isCaught;
	int hp;
	int attack;
	int defence;
	int specialAttack;
	int specialDefence;
	int speed; 
	string nature;
	string ability;
	string typeOne;
	string typeTwo;
	string moveOne;
	string moveTwo;
	string moveThree;
	string moveFour;
}

public class PokemonStructScript : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
