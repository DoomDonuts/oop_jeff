﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Stats
{
	public int health;
	public string name;
	public float movementSpeed;
	public float jumpHeight;
	public Keybinds keyBinds;
}

public class Player : MonoBehaviour 
{
	public Stats stats;
	public Keybinds keyBinds;
	public Rigidbody myRB;
	
	// Use this for initialization
	void Start () 
	{
		myRB = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		playerMovement();
	}
	
	public void playerMovement()
	{
		if(Input.GetKey(keyBinds.forwardKey))
		{
			myRB.AddForce (Vector3.forward * stats.movementSpeed);
		}
		
		if(Input.GetKey(keyBinds.leftKey))
		{
			myRB.AddForce (Vector3.left * stats.movementSpeed);
		}
		
		if(Input.GetKey(keyBinds.rightKey))
		{
			myRB.AddForce (Vector3.right * stats.movementSpeed);
		}
		
		if(Input.GetKey(keyBinds.backKey))
		{
			myRB.AddForce (Vector3.back * stats.movementSpeed);
		}
	}
}
