﻿using UnityEngine;
using System.Collections;

public class ArrayTutorial : MonoBehaviour 
{

	public string[] studentNames; //The array is a variable
	public int arraySize;

	// Use this for initialization
	void Start () 
	{
		SetNames();
		PrintNames ();
		TestLoop ();
		studentNames = new string[arraySize];
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void SetNames()
	{
      //		studentNames [0] = "John"; //sets the value of element 0 to the string "John"
      //		studentNames [1] = "Jeff";
      //		studentNames [2] = "Steve";
      //		studentNames [3] = "Kiri";
      //		studentNames [4] = "Alice";
      //		studentNames [5] = "Tristen";
	 for (int index = 0; index < studentNames.Length; index++) //.Length automatically uses the size of the array
	   {
			studentNames[index] = "Student No " + index;
	   }
	}

	public void PrintNames()
	{
//		print (studentNames [0]); //prints the value contained in element 0, in this case its the string "John"
//		print (studentNames [1]);
//		print (studentNames [2]);
//		print (studentNames [3]);
//		print (studentNames [4]);
//		print (studentNames [5]);
		for(int index = 0; index < studentNames.Length; index++)
		{
			print (studentNames[index]);
		}
	}

	public void TestLoop()
	{
		for(int index = 0; index < 10; index++) //index++ is the same as index += 1
		{
			print (index);
			//index--;   - this negates the post loop operation, creating an infinite loop
		}
	}
}
