﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct LoginInfo
{
	public string loginName;
	public string password;
	public string personalMessage;
	public int age;
}

public class CharacterRegistrator : MonoBehaviour 
{

	public LoginInfo[] players;


	// Use this for initialization
	void Start () 
	{
		PrintLoginInfo ();
		SettingLoginInfo ();
		SabotageLastPlayer ();

	}

	public void PrintLoginInfo()
	{
		print (players[0]); //this accesses the entire element
		print (players[0].loginName); //this accesses the login name from the element
		print (players[0].password); //this accesses the password from the element
		print (players[0].personalMessage); //this accesses the personal SendMessage from the element
		print (players[0].age); //this accesses the age of the element
	}

	public void SettingLoginInfo()
	{
		//this function will set the values of the last element of the LoginInfo array
		/*players [2].loginName = "Lydia"; //i tried using players[players.Length].loginName = "Lydia"; , but it returned the "index out of range" error.
		players [2].password = "umbrella";
		players [2].personalMessage = "Pepsi > Coke";
		players [2].age = 50;*/ //my original attempt

		players [players.Length - 1].loginName = "Lydia";
		players [players.Length - 1].password = "umbrella";
		players [players.Length - 1].personalMessage = "Pepsi > Coke";
		players [players.Length - 1].age = 50;
	}

	public void SabotageLastPlayer()
	{
		//make the last player's login info blank
		players [players.Length - 1].loginName = " "; 
		players [players.Length - 1].password = " ";
		players [players.Length - 1].personalMessage = " ";
		players [players.Length - 1].age = 0;
	}
}
