﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct StructScope
{					  //start of the struct scope
	public int myInt; //created within the struct scope
	public bool myBool; //created within the struct scope
	public string notMyString; //created within the struct scope
}                     //end of the struct scope

public class ScopeTutorial : MonoBehaviour 
{
	public int classNumber; //visible due to it being declared in the class scope
	public bool exists;
	public string messageInABottle;
	public bool checkIf;
	public StructScope myStruct; //created within the class scope of ScopePractice
	
	private string secretMessage = "This is a secret message";
	

	// Use this for initialization
	void Start () 
	{
		bool exists;
		exists = true;
		this.exists = exists;
		ScopeFunction();
		
		//modified within the start scope, and accesses the struct's scope via the '.' operator
		myStruct.myBool = true; //
		myStruct.myInt = 9001;
		myStruct.notMyString = "Hello World";
	}
	
	// Update is called once per frame
	void Update () 
	{
		string messageInABottle;
		messageInABottle = "Message";
		this.messageInABottle = messageInABottle;
	}
	
	public void ScopeFunction()
	{
		bool checkIf;
		checkIf = true;
		this.checkIf = checkIf;
		
		if(checkIf)
		{
			string myString;
			myString = "Hello World";
		}
	}
	
	public void PrintSecretMessage()
	{
		print(secretMessage);
	}
	
	public void ChangeSecretMessage(string newMessage)
	{
		if(newMessage.Length > 0 && newMessage.Length < 10)
		{
			secretMessage = newMessage;
		}
		else
		{
			Debug.Log("Warning! you tried to change the secret message incorrectly!");
		}
	}
	
	public string GetSecretMessage()
	{
		return secretMessage;
	}
}
