﻿using UnityEngine;
using System.Collections;

public class ScopeModifier : MonoBehaviour 
{
	public ScopeTutorial otherClass;
	public string messageCopy;

	// Use this for initialization
	void Start () 
	{
		otherClass.ScopeFunction();
		
		otherClass.classNumber = 500;
		otherClass.checkIf = false;
		otherClass.exists = true;
		otherClass.messageInABottle = "This is a message";
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Return))
		{
			otherClass.PrintSecretMessage();
		}
		
		if(Input.GetKeyDown(KeyCode.Backspace))
		{
			otherClass.ChangeSecretMessage("coke");
		}
		
		if(Input.GetKeyDown(KeyCode.Backslash))
		{
			messageCopy = otherClass.GetSecretMessage();
		}
	}
}
