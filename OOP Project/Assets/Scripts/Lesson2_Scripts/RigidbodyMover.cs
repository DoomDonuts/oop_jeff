﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Keybinds
{
	public KeyCode leftKey;
	public KeyCode rightKey;
	public KeyCode forwardKey;
	public KeyCode backKey;
}

public class RigidbodyMover : MonoBehaviour 
{
	public float playerSpeed = 0.0f;
	private Rigidbody myRigidbody;
	public KeyCode left;
	public KeyCode right;
	public KeyCode forward;
	public KeyCode back;
	
	public Keybinds keyBinds;

	// Use this for initialization
	void Start () 
	{
		myRigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		movement ();
	}

	public void movement()
	{
		if (Input.GetKey (keyBinds.leftKey)) 
		{
			movePlayerLeft();
		}
		
		if (Input.GetKey (keyBinds.rightKey)) 
		{
			movePlayerRight();
		}
		
		if (Input.GetKey (keyBinds.forwardKey)) 
		{
			movePlayerForward();
		}
		
		if (Input.GetKey (keyBinds.backKey)) 
		{
			movePlayerBack();
		}
	}

	public void movePlayerLeft()
	{
		print ("player is moving left with rigidbody.addforce");
		myRigidbody.AddForce (Vector3.left * playerSpeed);
	}

	public void movePlayerRight()
	{
		print ("player is moving right with rigidbody.addforce");
		myRigidbody.AddForce (Vector3.right * playerSpeed);
	}

	public void movePlayerForward()
	{
		print ("player is moving forward with rigidbody.addforce");
		myRigidbody.AddForce (Vector3.forward * playerSpeed);
	}

	public void movePlayerBack()
	{
		print ("player is moving back with rigidbody.addforce");
		myRigidbody.AddForce (Vector3.back * playerSpeed);
	}
}
