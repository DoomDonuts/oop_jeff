﻿using UnityEngine;
using System.Collections;

public class FunctionTutorial : MonoBehaviour 
{
	public string myName;
	public int myAge;
	public bool isMale;
	public string randomSentence;

	private Rigidbody myRigidbody;
	public float jumpForce = 0.0f;


	// Use this for initialization
	void Start () 
	{
		printMyName ();
		//printID ();
		myRigidbody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		//checkKeyPresses ();
	}

	public void printMyName() //it is a good convention to add the scope to functions you create, to distinguish them from functions that unity creates.
	{
		print ("Jeff Tremble");
	}

	public void printID()
	{
		print (myName);
		print (myAge);
		print ("I am Male = " + isMale);
		print (randomSentence);
	}

	public void checkKeyPresses()
	{
		//input.getkey = holding the key down
		//input.getkeydown = pressing the key once
		if (Input.GetKey (KeyCode.W)) 
		{
			print ("W key was pressed");
			movePlayerForward();
		}

		if(Input.GetKey(KeyCode.A))
		{
			print ("A key was pressed");
			movePlayerLeft();
		}

		if(Input.GetKey(KeyCode.S))
		{
			print ("S key was pressed");
			movePlayerBack();
		}

		if(Input.GetKey(KeyCode.D))
		{
			print ("D key was pressed");
			movePlayerRight();
		}

		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			print ("Space key was pressed");
			playerJump();
		}
	}

	public void movePlayerForward()
	{ 
        print ("Player is moving forward");
		transform.Translate(Vector3.forward * Time.deltaTime);
	}

	public void movePlayerBack()
	{
		print ("Player is moving back");
		transform.Translate(Vector3.back * Time.deltaTime);
	}

	public void movePlayerLeft()
	{
		print ("Player is moving left");
		transform.Translate(Vector3.left * Time.deltaTime);
	}

	public void movePlayerRight()
	{
		print ("Player is moving right");
		transform.Translate(Vector3.right * Time.deltaTime);
	}

	public void playerJump()
	{
		print ("Player is jumping");
		myRigidbody.AddForce (Vector3.up * jumpForce);
	}
}
