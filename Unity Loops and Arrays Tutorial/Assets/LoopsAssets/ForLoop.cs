﻿using UnityEngine;
using System.Collections;

public class ForLoop : MonoBehaviour 
{
	public int numEnemies = 3;
	
	public void Start ()
	{
		for(int i = 0; i < numEnemies; i++)
		{
			Debug.Log("Creating enemy number: " + i);
		}
	}
}
