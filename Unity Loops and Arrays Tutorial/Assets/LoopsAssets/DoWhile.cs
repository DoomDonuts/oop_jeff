﻿using UnityEngine;
using System.Collections;

public class DoWhile : MonoBehaviour 
{
	public bool shouldContinue;

	void Start()
	{
		shouldContinue = false;
		
		do
		{
			print ("Hello World");
			
		}while(shouldContinue == true);
	}
}
