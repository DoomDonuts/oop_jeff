﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KeyCollectionScript : MonoBehaviour 
{
	public GameObject keyImage; //change the design plan for this variable to a GameObject
	public GameObject keyCollectedText; //change the design plan for this variable to a GameObject
	public string playerTag;
	
	public DoorScript doorScript;

	// Use this for initialization
	void Start () 
	{
		keyImage.SetActive(false);
		keyCollectedText.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Rotate(Vector3.up * 2f );
	}
	
	public void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == playerTag)
		{
			//store key(activate image, set bool to true)
			keyCollectedText.SetActive(true);
			keyImage.SetActive(true);
			doorScript.isKeyCollected = true;
			Destroy (this.gameObject);
		}
	}
}
