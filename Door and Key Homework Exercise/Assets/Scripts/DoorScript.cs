﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour 
{
	public GameObject doorOpenedText;
	public string playerTag; //change doorTag to playerTag in the design
	public GameObject keyImage; //add this to the design document
	public GameObject keyCollectedMessage; //add this to the design document
	public bool isKeyCollected; //change this in the design to this script, reference form key script
	public Animator doorAnim;	

	// Use this for initialization
	void Start () 
	{
		doorOpenedText.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	public void OnTriggerStay(Collider other)
	{
		if(Input.GetMouseButtonDown(0) && other.gameObject.tag == playerTag && isKeyCollected)
		{
			doorAnim.SetBool("CanOpen", true);
			doorOpenedText.SetActive(true);
			isKeyCollected = false;
			keyImage.SetActive(false);
			keyCollectedMessage.SetActive(false);
		}
	}
}
